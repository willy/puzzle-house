# Puzzle House
<img src="https://user-images.githubusercontent.com/22482325/140875712-7ab24fba-04ea-4e80-8fd4-99c6c4cb5199.png" />
Desafiate recorriendo las distintas habitaciones y poniendo a prueba tu capacidad para resolver puzzles

## Niveles

<details>
 	<summary>Nivel 0 Puzzle inicio</summary>
	<img src="https://github.com/algo1unsam/tpgame-juegoooo/blob/master/nivel1.png?raw=true" />
</details>

<details>
	<summary>Nivel 1 Exploracion de la casa del personaje principal</summary>
	<img src="https://github.com/algo1unsam/tpgame-juegoooo/blob/master/mapa.png?raw=true" />
</details>

<details>
	<summary>Nivel 2 Puzzle</summary>
	<img src="https://github.com/algo1unsam/tpgame-juegoooo/blob/master/mapaW.png?raw=true" />
</details>

<details>
	<summary>Nivel 3 Puzzle</summary>
	<img src="https://github.com/algo1unsam/tpgame-juegoooo/blob/master/mapaBel.png?raw=true" />
</details>

<details>
	<summary>Nivel 4 Puzzle</summary>
	<img src="https://github.com/algo1unsam/tpgame-juegoooo/blob/master/mapaL.png?raw=true" />
</details>
	
## Equipo de desarrollo
- Maria Belen Rodriguez Schachtschneider
- Lorenzo Julián Martinez
- Wilfredo Joshua Bardales Castillo


## Reglas de Juego / Instrucciones

El juego consiste en ir recorriendo las distintas habitaciones,por medio de las flechas de direccionamiento del teclado y, al ingresar en cada habitación y, acercarse a
la persona que está en la misma, se desbloquea un nuevo puzzle el cual debe resolverse para liberar al personaje.
Para resolver el puzzle todas las cajas deben posicionarse en la meta correspondiente a su color y, para moverlas al igual que el personaje, es con las flechas de 
direccionamiento del teclado

## Otros

- Algoritmos I/Unsam
- Versión de wollok 3.0.0
- Una vez terminado, no tenemos problemas en que el repositorio sea público
